/**
 * Each test will be run against Lite modules imported from:
 *   - the source code, /src/lite/<module>.js
 *   - the module-aggregators, /src/lite.js and /src/u3test.js
 *   - the build files, /bundles/lite.*.js and /bundles/u3test.*.js
 *
 * This file imports Lite modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as an object.
 *
 * That object will be aggregated by /tests/u3test-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3test/tests/lite-tests
 */


// foo
import foo_src                   from '../src/lite/foo.js' // source
import { foo as foo_src_agg }    from '../src/lite.js' // aggregator
import { foo as foo_src_u3test } from '../src/u3test.js'
import { foo as foo_esm }        from '../bundles/lite.esm.js'
import { foo as foo_esm_min }    from '../bundles/lite.esm.min.js'
import { foo as foo_u3_esm }     from '../bundles/u3test.esm.js'
import { foo as foo_u3_esm_min } from '../bundles/u3test.esm.min.js'
const foo_modules = [
    [ foo_src,        '../src/lite/foo.js' ],
    [ foo_src_agg,    '../src/lite.js' ],
    [ foo_src_u3test, '../src/u3test.js' ],
    [ foo_esm,        '../bundles/u3test.esm.js' ],
    [ foo_esm_min,    '../bundles/u3test.esm.min.js' ],
    [ foo_u3_esm,     '../bundles/u3test.esm.js' ],
    [ foo_u3_esm_min, '../bundles/u3test.esm.min.js' ],
]
import foo_test_1 from './lite-tests/foo-typical-usage.js'
const foo_tests = [
    foo_test_1,
]


// Pass to /tests/u3test-tests.js, which will aggregate all U3Test tests.
export default {
    title: 'Lite',
    tests:[
        { title:'foo', modules:foo_modules, tests:foo_tests },
    ],
}

/**
 * Each test will be run against Bench modules imported from:
 *   - the source code, /src/bench/<module>.js
 *   - the module-aggregators, /src/bench.js and /src/u3test.js
 *   - the build files, /bundles/bench.*.js and /bundles/u3test.*.js
 *
 * This file imports Bench modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as an object.
 *
 * That object will be aggregated by /tests/u3test-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3test/tests/bench-tests
 */


// bar
import bar_src                   from '../src/bench/bar.js' // source
import { bar as bar_src_agg }    from '../src/bench.js' // aggregator
import { bar as bar_src_u3test } from '../src/u3test.js'
import { bar as bar_esm }        from '../bundles/bench.esm.js'
import { bar as bar_esm_min }    from '../bundles/bench.esm.min.js'
import { bar as bar_u3_esm }     from '../bundles/u3test.esm.js'
import { bar as bar_u3_esm_min } from '../bundles/u3test.esm.min.js'
const bar_modules = [
    [ bar_src,        '../src/bench/bar.js' ],
    [ bar_src_agg,    '../src/bench.js' ],
    [ bar_src_u3test, '../src/u3test.js' ],
    [ bar_esm,        '../bundles/bench.esm.js' ],
    [ bar_esm_min,    '../bundles/bench.esm.min.js' ],
    [ bar_u3_esm,     '../bundles/u3test.esm.js' ],
    [ bar_u3_esm_min, '../bundles/u3test.esm.min.js' ],
]
import bar_test_1 from './bench-tests/bar-normal-usage.js'
import bar_test_2 from './bench-tests/bar-incorrect-usage.js'
const bar_tests = [
    bar_test_1,
    bar_test_2,
]


// Pass to /tests/u3test-tests.js, which will aggregate all U3Test tests.
export default {
    title: 'Bench',
    tests:[
        { title:'bar', modules:bar_modules, tests:bar_tests },
    ],
}

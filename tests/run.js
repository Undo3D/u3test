import test from './u3test-tests.js'

const assert = {
    equal (a, b) { if (a !== b) throw Error(`'${a}' !== '${b}'`) },
}
// console.log( JSON.stringify(run(test),null,' ') )

if ('object' === typeof document)
    document.querySelector('#test-results').innerHTML = format( run(test) )
else
    console.log( format(run(test)) )

// The argument can be one of three levels of test:
//   - Branch:  { title:'Huge Scope', tests:[ ... ] }
//   - Twig:    { title:'Medium Scope', modules:[ ... ], tests:[ ... ] }
//   - Leaf:    { title:'Small Scope', test:(it, assert) => { ... } }
function run ({ title, modules, tests, test }) {
    validateTest({ title, modules, tests, test })

    if (tests && ! modules)
        return runBranch(title, tests)

    return runTwig(title, modules, tests)
}

function validateTest ({ title, modules, tests, test }) {
    if (! title) throw Error('No title')
    if (tests && test) throw Error(`'${title}' has \`tests\` and \`test\``)
    if (modules && test) throw Error(`'${title}' has \`modules\` and \`test\``)
}

function runBranch (title, tests) {
    const results = tests.map(run)
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runTwig (title, modules, tests) {
    const results = tests.map( leafTest => runLeaf(leafTest, modules) )
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runLeaf ({ title, test }, modules) {
    const results = modules.map(
        ([ module, modulePath ]) => runLeafWithModule(test, module, modulePath)
    )
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runLeafWithModule (test, module, modulePath) {
    try {
        test(assert, (title, fn) => {
            try {
                fn(module)
            } catch (err) {
                throw { title:'it '+title, status:'fail', modulePath, err }
            }
        })
    } catch (fail) {
        return fail
    }
    return { title:'ok', status:'pass', modulePath }
}

function format ({ title, status, results, modulePath, err }, depth=0) {
    validateResult (title, status, results, modulePath, err)
    if ('pass' === status)
        return depth ? '' : `Passed all tests`
    const pad = '  '.repeat(depth)
    const out = results
        ? formatBranch(results, depth)
        : formatTwig(pad, modulePath, err)
    return (depth ? '\n' : '') + `${pad}${title}:${out}`
}

function formatBranch (results, depth) {
    return results.map( r => format(r, depth+1) ).filter(r=>r).join('')
}

function formatTwig (pad, modulePath, err) {
    return `\n${pad}  ${modulePath}\n${pad}  ${err.message}`
}

function validateResult (title, status, results, modulePath, err) {
    if (! title)
        throw Error('No title')
    if ('pass' !== status && 'fail' !== status)
        throw Error(`'${title}' has invalid status '${status}'`)
    if (results) {
        if (modulePath || err)
            throw Error(`'${title}' is ambiguous`)
    } else if (! modulePath || ! err)
        if ('pass' !== status)
            throw Error(`'${title}' is malformed`)
}

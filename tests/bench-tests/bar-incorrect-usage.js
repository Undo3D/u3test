export default {
    title: 'bar incorrect usage',
    test: (assert, it) => {
        it('returns "bar" when anything falsey is passed in', bar => {
            assert.equal(bar(null), 'bar')
            assert.equal(bar(0), 'bar')
        })
        it('returns "BAR" when anything truthy is passed in', bar => {
            assert.equal(bar([false]), 'BAR')
            assert.equal(bar(-1), 'BAR')
        })
    },
}

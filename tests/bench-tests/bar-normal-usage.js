export default {
    title: 'bar normal usage',
    test: (assert, it) => {
        it('returns "bar" when no arguments are passed', bar => {
            assert.equal(bar(), 'bar')
        })
        it('returns "BAR" or "bar" when `caps` is a Boolean', bar => {
            assert.equal(bar(true), 'BAR')
            assert.equal(bar(false), 'bar')
        })
    },
}

import bench_tests from './bench-tests.js'
import lite_tests from './lite-tests.js'

export default {
    title: 'U3Test',
    tests: [
        bench_tests,
        lite_tests,
    ],
}

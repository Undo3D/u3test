export default {
    title: 'foo typical usage',
    test: (assert, it) => {
        it('returns "foo" when no arguments are passed', foo => {
            assert.equal(foo(), 'foo')
        })
        it('returns "FOO" or "foo" when `caps` is a Boolean', foo => {
            assert.equal(foo(true), 'FOO')
            assert.equal(foo(false), 'foo')
        })
    },
}

// From bench.js
export { default as bar } from './bench/bar.js'

// From lite.js
export { default as foo } from './lite/foo.js'

//@TODO automatically concat this file as part of the `$ npm run build` process

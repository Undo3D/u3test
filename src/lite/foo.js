/**
 * Returns either 'foo' or 'FOO'. This is just a toy function, while we’re
 * setting up Lite.
 *
 * `Boolean -> String`
 * @param {Boolean} caps - whether to return the string in capital letters
 * @return {String} 'FOO' if `caps` is true, otherwise 'foo'
 *
 * @example
 *     import foo from 'u3test/src/lite/foo.js'
 *     foo() // => 'foo'
 *
 * @example
 *     import { foo } from 'u3test/bundles/u3test.esm.js'
 *     foo(true) // => 'FOO'
 *
 * @public
 * @module u3test/lite/foo
 */

export default caps => caps ? 'FOO' : 'foo'

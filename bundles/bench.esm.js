/* U3Test Bench 0.0.2 */
/**
 * Returns either 'bar' or 'BAR'. This is just a toy function, while we’re
 * setting up Bench.
 *
 * `Boolean -> String`
 * @param {Boolean} caps - whether to return the string in capital letters
 * @return {String} 'BAR' if `caps` is true, otherwise 'bar'
 *
 * @example
 *     import bar from 'u3test/src/bench/bar.js'
 *     bar() // => 'bar'
 *
 * @example
 *     import { bar } from 'u3test/bundles/u3test.esm.js'
 *     bar(true) // => 'BAR'
 *
 * @public
 * @module u3test/bench/bar
 */

const bar = caps => caps ? 'BAR' : 'bar';

export { bar };

/* U3Test 0.0.2 */
/**
 * Returns either 'bar' or 'BAR'. This is just a toy function, while we’re
 * setting up Bench.
 *
 * `Boolean -> String`
 * @param {Boolean} caps - whether to return the string in capital letters
 * @return {String} 'BAR' if `caps` is true, otherwise 'bar'
 *
 * @example
 *     import bar from 'u3test/src/bench/bar.js'
 *     bar() // => 'bar'
 *
 * @example
 *     import { bar } from 'u3test/bundles/u3test.esm.js'
 *     bar(true) // => 'BAR'
 *
 * @public
 * @module u3test/bench/bar
 */

const bar = caps => caps ? 'BAR' : 'bar';

/**
 * Returns either 'foo' or 'FOO'. This is just a toy function, while we’re
 * setting up Lite.
 *
 * `Boolean -> String`
 * @param {Boolean} caps - whether to return the string in capital letters
 * @return {String} 'FOO' if `caps` is true, otherwise 'foo'
 *
 * @example
 *     import foo from 'u3test/src/lite/foo.js'
 *     foo() // => 'foo'
 *
 * @example
 *     import { foo } from 'u3test/bundles/u3test.esm.js'
 *     foo(true) // => 'FOO'
 *
 * @public
 * @module u3test/lite/foo
 */

const foo = caps => caps ? 'FOO' : 'foo';

// From bench.js

//@TODO automatically concat this file as part of the `$ npm run build` process

export { bar, foo };
